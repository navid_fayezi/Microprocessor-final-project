/*
 * Final_Project.c
 *
 * Created: 19/07/2020 03:43:58 �.�
 * Author: Navid
 */

#include <io.h>
#include <mega32.h>
#include <delay.h>
#include <stdio.h>
#include <alcd.h>
#include <stdlib.h>
//#include <stdint.h>

struct Clock
{
    char hour;
    char minute;
};


enum SYSTEM_STATE {UNLOCKED , LOCKED};
enum SYSTEM_SETTING {AUTO_CONF, MANUAL_CONF};
enum LIGHT_LEVEL { MANUAL_LEVEL, AUTO_LEVEL};
enum LIGHT_SETTING { MANUAL_SWITCH, AUTO_SWITCH};
enum AC_SETTING { MANUAL_AC, AUTO_AC};
enum WINDOW_SETTING { MANUAL_CONTROL, AUTO_CONTROL};



enum LIGHT_LEVEL lightlevel = AUTO_LEVEL ;
enum LIGHT_SETTING lightsetting = AUTO_SWITCH;
enum AC_SETTING acsetting = AUTO_AC;
enum WINDOW_SETTING windowsetting = AUTO_CONTROL;
enum SYSTEM_SETTING systemsetting = AUTO_CONF;
enum SYSTEM_STATE systemstate  = LOCKED;


char setIllumination(int adcin);
void showOnLCD(int x, int y, int temp);
void lightswitch( struct Clock from, struct Clock to, enum LIGHT_SETTING setting);
void showtemp();
void showtime();
void acandwindow(char t_low, char t_up, char h_low, char h_up);



// interfacing sensors with processor
#define DHT11_PIN 6
unsigned char c=0,I_RH,D_RH,I_Temp,D_Temp,CheckSum;


void Request()                /* Microcontroller send start pulse/request */
{
    DDRD |= (1<<DHT11_PIN);
    PORTD &= ~(1<<DHT11_PIN);    /* set to low pin */
    delay_ms(20);            /* wait for 20ms */
    PORTD |= (1<<DHT11_PIN);    /* set to high pin */
}

void Response()                /* receive response from DHT11 */
{
    DDRD &= ~(1<<DHT11_PIN);
    while(PIND & (1<<DHT11_PIN));
    while((PIND & (1<<DHT11_PIN))==0);
    while(PIND & (1<<DHT11_PIN));
}

unsigned char Receive_data()            /* receive data */
{   
    int q; 
    for (q=0; q<8; q++)
    {
        while((PIND & (1<<DHT11_PIN)) == 0);  /* check received bit 0 or 1 */
        delay_us(30);
        if(PIND & (1<<DHT11_PIN))/* if high pulse is greater than 30ms */
		c = (c<<1)|(0x01);	/* then its logic HIGH */
		else			/* otherwise its logic LOW */
		c = (c<<1);
		while(PIND & (1<<DHT11_PIN));
	}
	return c;
}

/////////////////////




void setClock(int h , int m, struct Clock* clock){
    // set time
    clock->hour = h % 24;
    clock->minute = m % 60;
}


void addHour(struct Clock* clock){
    // add one hour to current time
    clock->hour = (clock->hour + 1) % 24;
}


void addMinute(struct Clock* clock){
    // add one minute to current time
    clock->minute = (clock->minute + 1) % 60;
    if(clock->minute == 0)
        addHour(clock);
}



struct Clock* clockSub(struct Clock a, struct Clock b){
    // a - b
    struct Clock* p = (struct Clock*)malloc(sizeof(struct Clock));
    p->hour = a.hour;
    p->minute = a.minute;
    if(p->minute < b.minute){
        p->minute += 60;
        p->hour = p->hour - 1;
        }
        
    p->minute = p->minute - b.minute;
    p->hour = p->hour - b.hour;
    return p;
}

int isBetween(struct Clock from,struct Clock to, struct Clock clock){
    
    struct Clock *temp ;
    char clockstr[2];
    if((signed char)from.hour > (signed char)to.hour || ((signed char)from.hour == (signed char)to.hour && (signed char)from.minute > (signed char)to.minute)){    
        temp = clockSub(from,clock);
        if(((signed char)temp->hour == (signed char)0 && (signed char)temp->minute == (signed char)0) || ((signed char)temp->hour < (signed char)0))
            return 1;
        
        else{
            temp = clockSub(clock,to);
            if((signed char)temp->hour < (signed char)0){
                return 1;        
                }
            return 0;
        }    
            
        }
    else{

        temp = clockSub(to,clock); 
        if(((signed char)temp->hour == (signed char)0 && (signed char)temp->minute ==(signed char)0) || ((signed char)temp->hour < (signed char)0)){
            return 0;
        }
        else{
            temp = clockSub(clock,from);
            if((signed char)temp->hour < (signed char)0)
                return 0;
            return 1;
        }   
    }    
}


static struct Clock myclock;
static struct Clock switchon;
static struct Clock switchoff;

unsigned char keypadPatterns[12] = {0x7d,0xbd,0xdd,0xed,0x7b,0xbb,0xdb,0xeb,0x77,0xb7,0xd7,0xe7}; // 10 -> * , 11 -> #

int light;
int light_level;
int window1;
int window2;
int cooling_system;
int heating_system;
int new_key;
int pressed_key;
unsigned long int pressed_keys;

void main(void)
{   
    light = 0;    
    new_key = 0;
    window1 = 0;
    window2 = 0;
    cooling_system = 0;
    heating_system = 0;
    light_level = 10;
    setClock(0,0,&myclock);
    setClock(0,1,&switchon);
    setClock(0,2,&switchoff);
     
    // enable global Interrupts
    #asm("sei");  
    
    
    // initializing lcd
    lcd_init(40);
    lcd_clear();
                               
    // port A settings, connected to keypad
    DDRA = 0xf0;
    PORTA |= 0x0e;
    PORTA &= 0x0f;              
     
    // port D, connected to INT0  
    DDRD = 0xbb;
    PORTD = 0x44;
    

    DDRB = 0xff;
    
    // faling edge trigger for INT0
    MCUCR |= (0 << ISC00) | ( 1 << ISC01) ; 
    
    // enable INT0 locally 
    GIMSK |= (1 << INT0 );
    
    // enable timer counter 1 and timer counter 0 interrupt
    TIMSK |= 1 << TOIE1 | 1<<TOIE0 ;
    
    // enable timer counter, set tcnt to 1B1E so that overflow happens in 1 minute, prescaler -> 1024  
    TCCR1B = 0x05 ;                         
    TCNT1L = 0x1E;                          
    TCNT1H = 0x1B; 
    
    // timer counter 0 for sensors
    TCCR0 = 0x05;
    TCNT0 = 0;
    
    // ADC
    // ADMUX = 0;
    ADCSRA = (1 << ADEN) | (1 << ADSC) | (1 << ADIE) | ( 1 << ADATE);
    ADCSRA |= 0b00000111;
    
    
   
    

    
    while(1){
        if(systemsetting == AUTO_CONF){
            lcd_clear();
            lcd_gotoxy(0,0);
            lcd_putsf("1.Temp. and humidity 2.windows 3.AC unit 4.Light 5.time 6.light settings");
            while(new_key == 0);
            new_key = 0;
            if(pressed_key == 1){
                lcd_clear();
                lcd_gotoxy(0,0);
                showtemp(); 
                delay_ms(10);
                while(new_key == 0);
                new_key = 0;
                continue;
                }
            if(pressed_key == 2){
                // window1
                lcd_clear();
                lcd_gotoxy(0,0);
                lcd_putsf("w1: ");
                showOnLCD(5,0,window1);  
                // window 2
                lcd_gotoxy(0,1);
                lcd_putsf("w2: ");
                showOnLCD(5,1,window2); 
                delay_ms(10);
                while(new_key == 0);
                new_key = 0;
                continue;
                }
            if(pressed_key == 3){
                lcd_clear();
                lcd_gotoxy(0,0);
                lcd_putsf("cooling sys.: ");
                showOnLCD(20,0,cooling_system);
                lcd_gotoxy(0,1);
                lcd_putsf("heating sys.: ");
                showOnLCD(20,1,heating_system);
                delay_ms(10);  
                while(new_key == 0);
                new_key = 0;
                continue;
                }
            if(pressed_key == 4){
                lcd_clear();
                lcd_gotoxy(0,0);
                lcd_putsf("light level: ");
                showOnLCD(17,0,light_level);
                delay_ms(10); 
                while(new_key == 0);
                new_key = 0;
                continue;
                }
            if(pressed_key == 5){
                lcd_clear();
                lcd_gotoxy(0,0);
                showtime();
                delay_ms(10); 
                while(new_key == 0);
                new_key = 0;
                continue;
            }
            if(pressed_key == 6){
                char data[3];
                lcd_clear();
                lcd_gotoxy(0,0);
                    
                  
                lcd_putsf("turn the light on : ");
                itoa(switchon.hour,data);
                lcd_puts(data);
                itoa(switchon.minute, data);
                lcd_putsf(":");
                lcd_puts(data);
                lcd_putsf("  ");
                    
                lcd_gotoxy(0,1);
                lcd_putsf("turn the light off : ");
                itoa(switchoff.hour,data);
                lcd_puts(data);
                itoa(switchoff.minute, data);
                lcd_putsf(":");
                lcd_puts(data);
                lcd_putsf("  ");
                    
                delay_ms(10); 
                while(new_key == 0);
                new_key = 0;
                continue;
            }
            if(pressed_key == 10){
                systemsetting = MANUAL_CONF;
                continue;
            }
                          
        }
        else{
            
            if(systemstate == LOCKED){
                
                int counter = 0;
                new_key = 0;
                pressed_key = 0;
                lcd_clear();
                lcd_gotoxy(0,0);
                lcd_putsf("Enter the Password");
                pressed_keys = 0;
                lcd_gotoxy(0,1);
                while(pressed_key != 11 && counter < 10){
                                                
                    while(new_key == 0);
                    new_key = 0;
                    if(pressed_key != 11)
                        pressed_keys = pressed_keys * 10 + pressed_key;
                    lcd_putsf("*");
                    counter++;        
                    }
                if(pressed_keys == 9)
                    systemstate = UNLOCKED;
            }
            else{
                new_key = 0;
                lcd_clear();
                lcd_gotoxy(0,0);
                lcd_putsf("1.light setting 2.ac setting            3.windows setting 4.auto configuration");
                
                while(new_key == 0);
                new_key = 0;
                if(pressed_key == 1){
                    lcd_clear();
                    lcd_gotoxy(0,0);
                    lcd_putsf("1.turn off/on 2.auto light level on/off 3.auto switch on/off4.switch on/off time"); 
                    delay_ms(10);
                    while(new_key == 0);
                    new_key = 0;
                    if(pressed_key == 1){
                        lcd_clear();
                        lcd_gotoxy(0,0);
                        lightsetting = MANUAL_SWITCH;
                        if(light == 1){
                            PORTD.5 = 0;
                            light = 0;
                            lcd_putsf("light turned off");
                            }
                        else{
                            PORTD.5 = 1;
                            light = 1;
                             lcd_putsf("light turned on");
                            }    
                        }
                    
                    if(pressed_key == 2){
                        
                        lcd_clear();
                        lcd_gotoxy(0,0);
                        if(lightlevel == AUTO_LEVEL){
                            lightlevel = MANUAL_LEVEL;
                            light_level = 0 + light * 9;
                            lcd_putsf("illumination level is set to manual. Enter a number (0-9):");
                            new_key = 0;
                            while(new_key == 0);
                            new_key = 0;
                            light_level = (pressed_key % 10 ) * light;
                            }
                        else{
                            lightlevel = AUTO_LEVEL;
                            light_level = 10;
                            lcd_putsf("illumination level is set to auto.");
                            }
                        
                        }
                    
                    if(pressed_key == 3){
                        
                        lcd_clear();
                        lcd_gotoxy(0,0);
                        if(lightsetting == AUTO_SWITCH){
                            
                            lightsetting = MANUAL_LEVEL;
                            lcd_putsf("auto switch is off.");
                            }
                        else{
                            lightsetting = AUTO_LEVEL;
                            lcd_putsf("auto switch is on.");
                            }
                        
                    }
                    
                    if(pressed_key == 4){
                        char data[3];
                        int h,m;
                        int counter = 0;
                        h = 0;
                        m = 0;
                        
                        lcd_clear();
                        lcd_gotoxy(0,0);
                        lcd_putsf("Enter the time you want the light to turn on(seperate by *): ");
                        new_key = 0;
                        pressed_keys = 0;
                        
                        lcd_gotoxy(30,1);
                        while(counter < 2){
                                                
                            while(new_key == 0);
                            new_key = 0;
                            if(pressed_key != 11 && pressed_key != 10){
                                pressed_keys = pressed_keys * 10 + pressed_key;
                                itoa(pressed_key,data);
                                lcd_puts(data);}
                            else{
                                break;
                                }
                            counter++;
                            
                                 
                        }
                        lcd_putsf(":");
                        h = pressed_keys % 24; 
                        counter = 0;
                        new_key = 0;
                        pressed_keys = 0;
                        while(counter < 2){
                                                
                            while(new_key == 0);
                            new_key = 0;
                            if(pressed_key != 11 && pressed_key != 10){
                                pressed_keys = pressed_keys * 10 + pressed_key;
                                itoa(pressed_key,data);
                                lcd_puts(data);}
                             else{
                                break;
                                }
                            counter++; 
                                   
                        } 
                        m = pressed_keys % 60;
                        setClock(h,m,&switchon);
                        
                        lcd_clear();
                        lcd_gotoxy(0,0);
                        lcd_putsf("Enter the time you want the light to turn off(seperate by *): ");
                        new_key = 0;
                        pressed_keys = 0;
                        counter = 0;
                        new_key =0;
                        lcd_gotoxy(30,1);
                        while(counter < 2){
                                                
                            while(new_key == 0);
                            new_key = 0;
                            if(pressed_key != 11 && pressed_key != 10){
                                pressed_keys = pressed_keys * 10 + pressed_key;
                                itoa(pressed_key,data);
                                lcd_puts(data);}
                             else{
                                break;
                                }
                            counter++; 
                             //lcd_gotoxy(30,1);
                                   
                        }
                        lcd_putsf(":");
                        h = pressed_keys % 24; 
                        counter = 0;
                        new_key = 0;
                        pressed_keys = 0;
                        while(counter < 2){
                                                
                            while(new_key == 0);
                            new_key = 0;
                            if(pressed_key != 11 && pressed_key != 10){
                                pressed_keys = pressed_keys * 10 + pressed_key;
                                itoa(pressed_key,data);
                                lcd_puts(data);}
                             else{
                                break;
                                }
                            counter++; 
                                   
                        } 
                        m = pressed_keys % 60;
                        setClock(h,m,&switchoff);
                        
                        
                        lcd_clear();
                        lcd_gotoxy(0,0);
                            
                          
                        lcd_putsf("turn the light on : ");
                        itoa(switchon.hour,data);
                        lcd_puts(data);
                        itoa(switchon.minute, data);
                        lcd_putsf(":");
                        lcd_puts(data);
                        lcd_putsf("  ");
                            
                        lcd_gotoxy(0,1);
                        lcd_putsf("turn the light off : ");
                        itoa(switchoff.hour,data);
                        lcd_puts(data);
                        itoa(switchoff.minute, data);
                        lcd_putsf(":");
                        lcd_puts(data);
                        lcd_putsf("  ");
                        
                    }
                    
                    while(new_key == 0);
                    new_key = 0;
                    continue;
                    }
                if(pressed_key == 3){
                    // window1
                    lcd_clear();
                    lcd_gotoxy(0,0);
                    lcd_putsf("1.open/close window 1");
                      
                    // window 2
                    lcd_gotoxy(0,1);
                    lcd_putsf("2.open/close window 2 3.auto setting on");
                    
                    new_key = 0;
                    pressed_key = 0;
                    while(new_key == 0);
                    new_key = 0;
                    if(pressed_key == 1){
                        window1 = 1 - window1;
                        windowsetting = MANUAL_CONTROL;
                        }
                    if(pressed_key == 2){
                        window2 = 1 - window2;
                        windowsetting = MANUAL_CONTROL;
                        }
                    if(pressed_key == 3){
                        windowsetting = AUTO_CONTROL;
                        }
                    lcd_clear();
                    lcd_putsf("press any key"); 
                    delay_ms(10);
                    while(new_key == 0);
                    new_key = 0;
                    continue;
                    }
                if(pressed_key == 2){
                    lcd_clear();
                    lcd_gotoxy(0,0);
                    lcd_putsf("1.turn on/off the cooling sys.");
                      
                    // window 2
                    lcd_gotoxy(0,1);
                    lcd_putsf("2.turn on/off the heating sys. 3.auto setting on");
                    
                    
                    new_key = 0;
                    pressed_key = 0;
                    while(new_key == 0);
                    new_key = 0;
                    if(pressed_key == 1){
                        cooling_system = (1 - cooling_system)*(1- heating_system);
                        acsetting = MANUAL_AC;
                        }
                    if(pressed_key == 2){
                        heating_system = (1 - heating_system) * (1-cooling_system);
                        acsetting = MANUAL_AC;
                        }
                    if(pressed_key == 3){
                        acsetting = AUTO_AC;
                        }
                    lcd_clear();
                    lcd_putsf("press any key"); 
                    delay_ms(10);  
                    while(new_key == 0);
                    new_key = 0;
                    continue;
                    }
                if(pressed_key == 4){
                    lcd_clear();
                    
                    lightlevel = AUTO_LEVEL ;
                    lightsetting = AUTO_SWITCH;
                    acsetting = AUTO_AC;
                    windowsetting = AUTO_CONTROL;
                    systemsetting = AUTO_CONF;
                    systemstate  = LOCKED; 
                    new_key = 0;
                    continue;
                    }
                
            }
            
        
        }
    }  
}


interrupt [TIM0_OVF] void timercounter0Overflow(void){
        
        Request();		/* send start pulse */
		Response();		/* receive response */
		I_RH=Receive_data();	/* store first eight bit in I_RH */
		D_RH=Receive_data();	/* store next eight bit in D_RH */
		I_Temp=Receive_data();	/* store next eight bit in I_Temp */
		D_Temp=Receive_data();	/* store next eight bit in D_Temp */
		CheckSum=Receive_data();/* store next eight bit in CheckSum */
		
        acandwindow(20,30,50,80);
        
        PORTD.0 = heating_system;
        PORTD.1 = cooling_system;
        PORTD.3 = window1;
        PORTD.4 = window2;
        lightswitch(switchon,switchoff,lightsetting); 
            
}


interrupt [TIM1_OVF] void timercounter1Overflow(void){
    
       
    TCNT1L = 0x1E;                          
    TCNT1H = 0x1B;
    addMinute(&myclock);
    
   
       
        
}

interrupt [EXT_INT0] void myInt(void){
    
    
    
    // find key
    int i,index;
    unsigned char pattern;  
    // delay_ms(20);   debounce
    
    new_key = 1;                                           
    DDRB = 0xff;
    
    DDRA |= 0xf0;                       // leaves porta.0 unchanged. porta.0 is connected to sensor and used by ADC.
    DDRA &= 0xf1;                       // same as above
    PORTA &= 0x0f;                       
    PORTA |= 0x0e;                      
    delay_us(5);
    pattern = (PINA & 0b00001111); 
    DDRA |= 0x0e;                        
    DDRA &= 0x0f;
    PORTA |= 0xf0;
    PORTA &= 0xf1; 
    delay_us(5); 
    pattern |= (PINA & 0b11110000) | 0x01;    // lsb is not connected to keypad, always gets the value 1
    for(i = 0 ; i < 12 ; i++){
        if(keypadPatterns[i] == pattern){
            pressed_key = i;
            break;
            }
    
    }
    //PORTB = pattern;
    //end of find key
   
    
    
}



interrupt [ADC_INT] void ADCint(void){
     
     //adc output ranges between 28 to 102
     int adcin;
     double ilevel;
     
     adcin = 0 | ADCL | (ADCH << 8);
     adcin &= 0x03ff;
     
     ilevel = setIllumination(adcin);
     if(lightlevel == AUTO_LEVEL && light == 1){
         
        light_level = 10;
         /*if(ilevel>0)
            PORTD.5 = 1;
         else
            PORTD.5 =0;
         lcd_gotoxy(10,0);
         lcd_putsf("light level :");
         showOnLCD(24,0,10);
        */}
        
     //showOnLCD(30,0,temp);
     // show illumination level of lamp on lcd
     //showOnLCD(35,0,ilevel);
     
     delay_ms(10);
     ADCSRA |= 1 << ADSC ;
      
}


char setIllumination(int adcin){
   
    char illumination_level = -1;
    double temp = (double)adcin;
         
    temp = temp - 28;
    
    while( temp >= 0){
        illumination_level++;
        temp = temp - 7.5;
        }
        
    return illumination_level;
}


void showOnLCD(int x, int y, int temp){
     
     char data[5];
     lcd_gotoxy(x,y);
     lcd_putsf("  ");
     lcd_gotoxy(x,y);  
     itoa(temp,data);
	 lcd_puts(data);
}

void lightswitch( struct Clock from, struct Clock to,enum LIGHT_SETTING setting){
    
    if(setting == AUTO_SWITCH){
        if(isBetween(from,to,myclock) == 1){
            PORTD.5 = 1;
            light = 1;
            
        }
        else{    
            PORTD.5 = 0;
            light = 0;
            }    
    } 

}


void showtemp(){
        
        char data[5];
        lcd_gotoxy(0,0);		
	    lcd_putsf("H =");
	    lcd_gotoxy(0,1);
	    lcd_putsf("T = ");
		

		
			
			itoa(I_RH,data);
			lcd_gotoxy(4,0);
			lcd_puts(data);
			lcd_putsf(".");
			
			itoa(D_RH,data);
			lcd_puts(data);
			lcd_putsf("%");

			itoa(I_Temp,data);
			lcd_gotoxy(4,1);
			lcd_puts(data);
			lcd_putsf(".");
			
			itoa(D_Temp,data);
			lcd_puts(data);
			


}


void showtime(){

    char data[3];
   // show time on lcd
    lcd_gotoxy(32,0);
    itoa(myclock.hour,data);
    lcd_puts(data);
    itoa(myclock.minute, data);
    lcd_putsf(":");
    lcd_puts(data);
    lcd_putsf("  ");
    
}


void acandwindow(char t_low, char t_up, char h_low, char h_up){
    
    if(I_RH<= h_up && I_RH >= h_low && I_Temp <= t_up && I_Temp >= t_low){
        if(acsetting == AUTO_AC){
             cooling_system = 0;
             heating_system = 0;
            }
        if(windowsetting == AUTO_CONTROL){
            window1 = 0;
            window2 = 0;
        }
    }
    
    if(I_RH > h_up && I_Temp <= t_up && I_Temp >= t_low){
        if(acsetting == AUTO_AC){
             cooling_system = 0;
             heating_system = 0;
            }
        if(windowsetting == AUTO_CONTROL){
            window1 = 0;
            window2 = 1;
        }
    }
    
    if(I_RH < h_low && I_Temp <= t_up && I_Temp >= t_low){
        if(acsetting == AUTO_AC){
             cooling_system = 0;
             heating_system = 0;
            }
        if(windowsetting == AUTO_CONTROL){
            window1 = 1;
            window2 = 1;
        }
    }
    
    if(I_Temp < t_low){
        if(acsetting == AUTO_AC){
             cooling_system = 0;
             heating_system = 1;
            }
        if(windowsetting == AUTO_CONTROL){
            window1 = 0;
            window2 = 0;
        }
    }    

    if(I_Temp > t_up){
        if(acsetting == AUTO_AC){
             cooling_system = 1;
             heating_system = 0;
            }
        if(windowsetting == AUTO_CONTROL){
            window1 = 0;
            window2 = 0;
        }
    } 
}